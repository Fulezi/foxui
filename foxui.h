/*
 * foxui.h - version 0.1.0 - Florian GOLESTIN
 * Started in April 2019
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 */

#ifndef SOLEIL__FOX_UI_H_
#define SOLEIL__FOX_UI_H_

/* TODO: Passe by value OR by pointer */
/* TODO: Currently, all method returns an error that must be checked. Except the
 * `_sub` ones. But that's not really handy. It would be better to have an error
 * management such as opengl. */

/* Hide OpenGL into the private implementation. */
#ifndef FOXUI_OPENGL_INCLUDE
#include <GL/gl.h>
#else
FOXUI_OPENGL_INCLUDE
#endif /* FOXUI_OPENGL_INCLUDE */

/* TODO: Include this the proper way */
/* TODO: Allow to bake the font and not include the stb */
#if !defined(FOXUI_STB_ALREADY_INCLUDED)
#define STB_RECT_PACK_IMPLEMENTATION
#define STBRP_STATIC
#include "assets/stb_rect_pack.h"
#include "assets/stb_truetype.h"
#endif

/* TODO: Allow to use own library */
#include <string.h>

#ifndef FOXUI_NO_EDITOR
#define FOXUI_EDITOR 1
#define FOXUI_EXTRA_WIDGETS_COUNT 32
#else
#define FOXUI_EDITOR 0
#endif

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef foxui_malloc
//# define foxui_malloc(size, user) ((void)user, malloc(size))
#define foxui_malloc(size) (malloc(size))
#endif

#ifndef foxui_free
//# define foxui_free(ptr, user) ((void)user, free(ptr))
#define foxui_free(ptr) (free(ptr))
#endif

#ifndef foxui_math
#include <math.h>

#define foxui_sin(angle) (sin(angle))
#define foxui_cos(angle) (cos(angle))
#endif

  /**
   * Absolute position defined by the top-left corner (`x` and `y`), and its
   * size (`width` and `height`)
   */
  typedef struct
  {
    float x;
    float y;
    float width;
    float height;

    float rot;
  } foxui_rectangle;

  /**
   * Multiply a rectangle position and size by the scalar `v`.
   */
  foxui_rectangle foxui_rectangle_mult(const foxui_rectangle r, const float v);

  /***
   * Compose two rectangles (post-multiplication).
   */
  foxui_rectangle foxui_rectangle_comp(const foxui_rectangle l,
                                       const foxui_rectangle r);

  /***
   * Inverse the transformation implied by a rectangle.
   */
  foxui_rectangle foxui_rectangle_inv(const foxui_rectangle l);

  /**
   * Vector 2 to represent a position (for instance a pivot point).
   */
  typedef struct
  {
    float x;
    float y;
  } foxui_vec2;

  foxui_vec2 foxui_sub_vec2_set(float x, float y);

  /**
   * All error types that can be returned byf foxui.
   * Can be API usage or system error.
   *
   * Any change in this table must be reported in foxui_strerror_table
   */
  enum
  {
    foxui_ret_success = 0,
    foxui_ret_opengl_error,
    foxui_ret_configuration_error,
    foxui_ret_io_error, /* Error while opening / reading a file. TODO: errno */
    foxui_ret_nomem     /* Out-of-memory */
  };

  /**
   * All major parts a widget can have. Represent a box.
   */
  enum
  {
    foxui_widgetpart_topleft = 0,
    foxui_widgetpart_top,
    foxui_widgetpart_topright,
    foxui_widgetpart_left,
    foxui_widgetpart_content,
    foxui_widgetpart_right,
    foxui_widgetpart_bottomleft,
    foxui_widgetpart_bottom,
    foxui_widgetpart_bottomright,

    foxui_widgetpart_count
  };

  /**
   * List of compile-time known widgets.
   */
  enum
  {
    foxui_widgetlist_textbox = 0,
    foxui_widgetlist_count,
  };

  typedef enum
  {
    /** The image will occupies all the space */
    foxui_image_extend,
    /** The image width will fit, its height will be resized (may exceed the
       layer). */
    foxui_image_fit_width,
    /** The image height will fit, its width will be resized (may exceed the
       layer). */
    foxui_image_fit_height,
    /** Fit as much as possible the space without changing the ratio of the
       image. */
    foxui_image_keep_ratio,
    foxui_image_true_size,
  } foxui_image_mode;

  /**
   * Look'n'Feel configuration of a widget.
   * Size are represented in percent [0, 1] of the widget.
   */
  typedef struct
  {
    foxui_rectangle* parts_size;
    foxui_rectangle* parts_uv;
    size_t           nb_parts;
  } foxui_widget_lnf;

  // TODO: All parts must be allocated from one vector
  typedef struct
  {
    foxui_rectangle parts_size;
    foxui_rectangle parts_uv;
  } foxui_widget_lnf2;

  /**
   * Define the configuration of the UI.
   */
  typedef struct
  {
    // TODO: Shouldn't it be in render?
    int viewport_width;
    int viewport_height;

    /** List of the available looks'n'feels. */
    foxui_widget_lnf* lnf_list;
    size_t            lnf_list_count;

    /** Custom user images, ordered by ID */
    foxui_rectangle* user_images;
    size_t           user_image_count;

    /** Size of the lnf list. Actually this list should be the size of the
     * number of available widget to user. */
    // size_t lnf_list_size;

    /** Atlas Texture loaded in OpenGL */
    GLuint atlas_texture;

    /** width of the original image in pixel */
    float atlas_width;
    /** height of the original image in pixel */
    float atlas_height;

    /** Atlas Font loaded in OpenGL */
    GLuint atlas_font;

    /** SDF Configuration.
     * A SDF character has values that decrease from 1.0 (center of the
     * character) to 0.0 (end of it). The `width` will represent the filled part
     * of the character while the edge represents the size of gradation part.
     * [0] == Width, a value from 0-1 (usually around 0.5)
     * [1] == Edge, a value from 0-1 (usually around 0.01)
     */
    float sdf_values[2];
    int   sdfeasing;

    /** A line-gap adjustment. 1.0 to keep the font gap, < 0 to reduce it or > 0
     * to increase it. This is useful to adjust some font that have a wrong
     * line-gap. */
    float linegap;

#if FOXUI_EDITOR == 1
    /**
     * Extra widget added (total count).
     */
    size_t extra_widgets_count;

    /**
     * Names of the extra widgets
     */
    const char* extra_widget_names[FOXUI_EXTRA_WIDGETS_COUNT];

    short extra_widget_parts_count[FOXUI_EXTRA_WIDGETS_COUNT];

    foxui_widget_lnf2* extra_widget_parts;
#endif

  } foxui_param;

  /** Allow to crop part of the rendering. */
  typedef struct
  {
    /** Type of stencil to use. 0 Means disabled. */
    short type;

    /** Absolute position in the screen resolution. */
    foxui_vec2 position;

    union
    {
      /** Radius in pixels */
      float radius;
      /** Size width (x) and height (y) */
      foxui_vec2 size;
    } extent;
  } foxui_stencil;

  typedef struct
  {
    GLint   vertices_start;
    GLsizei count;

    /** true if the draw command must render a Signed Distance Field. */
    short sdf;

    /** In-use stencil */
    foxui_stencil stencil;
  } foxui_draw_command;

  /**
   * Represent a Glyph sizes and UVs
   */
  typedef struct
  {
    foxui_rectangle rect;
    foxui_rectangle uvs;

    float left_bearing;
    float advance;
    // float height;
  } foxui_glyph;


  typedef enum
  {
    foxui_button_released,
    foxui_button_just_pressed,
    foxui_button_pressed,
    foxui_button_just_released,

    foxui_button_state_count,
  } foxui_button_state;

  /**
   * Define a render. It may have multiple renders for one param.
   */
  typedef struct
  {
    /** Pointer to the parameter used to create this render context. */
    foxui_param* param;

    /** Computed projection matrix. Must be updated if the viewport change. */
    float projection_mat[16];

    /** Array of draw command. Might extend in case of needs. */
    foxui_draw_command* commands_array;
    size_t              commands_count;
    size_t              commands_size;

    /* TODO: Rename as layer */
    foxui_rectangle* widget_stack;
    size_t           widget_count;
    size_t           widget_size;

    /** Buffer of stored vertices */
    float* vertices_buffer;
    size_t vertices_count;
    size_t vertices_size;

    /* --- Stencil --- */
    /** Current stencil in use. */
    foxui_stencil current_stencil;

    /* --- Text Data --- */

    /** All loaded glyphs */
    foxui_glyph* glyphs;

    /** The loaded TTF file */
    unsigned char* ttf_buffer;
    stbtt_fontinfo info;

    /* --- OpenGL Data --- */

    /** Uniform Index of the Projection Matrix */
    GLint uniform_projection_mat;

    /** Uniform Index of the Atlas texture */
    GLint uniform_atlas_texture;

    /** Enable a SDF draw (used for text) */
    GLint uniform_sdf;
    GLint uniform_sdfeasing;

    /** Values for the SDF rendering
     * Is a type of vec4
     * 0/x == width
     * 1/y == edge
     */
    GLint uniform_sdf_values;

    GLint uniform_sdf_colour;

    /** Values for a circle stencil */
    GLint uniform_stencil_circle;

    GLuint glprogram;
    GLuint vao;
    GLuint vbo;


    /* --- Events data --- */
    foxui_vec2 mouse_pos;
    foxui_button_state primary_button;

  } foxui_renderdata;

  /**
   * An OpenGL texture
   */
  typedef GLint foxui_texture;

  /**
   * Create and initialize a new UI,
   * Save the `param` pointer into `render`. It also create the OpenGL resource,
   * so you have to check that OpenGL is in a valid state after calling this
   * function. Return 0 if success.
   */
  int foxui_create(foxui_param* param, foxui_renderdata* render);

  /**
   * Load a glyph atlas directly from a TTF file (using POSIX system calls).
   */
  int foxui_font_from_ttf_file(foxui_param* param, foxui_renderdata* render,
                               const char* ttf_file);

  /**
   * Render an external image (already initialized in OpenGL) in a fixed,
   * absolute, position.
   */
  int foxui_render_image_external(foxui_renderdata* render, foxui_texture tex,
                                  const foxui_rectangle position);

  /**
   * Do the render in OpenGL
   */
  int foxui_render(foxui_renderdata* render);

  /* --- Widgets library --- */

  /**
   * Render a static text box with a specified position and size.
   */
  int foxui_widget_textbox(foxui_renderdata* render, const foxui_rectangle* pos,
                           const char* text);

  /* --- Layouts --- */

  /** Returns the layer currently in use */
  foxui_rectangle foxui_last_layer(foxui_renderdata* render);

  /** Experiment on a table 12x6. This create a layer that start at `col` and
   * `row` and is sized (in number of cols and rows) by `width` and `height`.
   */
  int foxui_set_cell(foxui_renderdata* render, int col, int row, int width,
                     int height);

  /** Experiment on a table 12x6. This create a layer that start at `col` and
   * `row` and is sized (in number of cols and rows) by `width` and `height`.
   * Allow a rotation on the specified pivot (or NULL to be on the corner).
   */
  int foxui_set_cell_r(foxui_renderdata* render, int col, int row, int width,
                       int height, float r, foxui_vec2* pivot);

  int foxui_layer_push(foxui_renderdata* render, foxui_rectangle rec,
                       foxui_vec2* pivot);

  int foxui_push_relative_layer(foxui_renderdata* render, foxui_rectangle rec,
                                foxui_vec2* pivot);

  int foxui_layer_pop(foxui_renderdata* render);

  /** Return 1 if the mouse cursor is on the widget. 0 otherwise. */
  int foxui_layer_hovered(foxui_renderdata* render);

  /** Return 1 if the mouse cursor is on the widget and the primary button
   *  went from pressed to just_released on it. 0 otherwise. */
  int foxui_layer_clicked(foxui_renderdata *render);

  int foxui_layer_dragged(foxui_renderdata* render, float* factor);


/* --- Stencils --- */

int foxui_stencil_circle(foxui_renderdata *render, foxui_vec2 position,
                         float radius);

/** Use the current layer as stencil (we cannot draw out of it). */
int foxui_stencil_layer(foxui_renderdata *render);

int foxui_stencil_square(foxui_renderdata *render, const foxui_rectangle rec);

int foxui_stencil_reset(foxui_renderdata *render);

enum {
  /** We are in the stencil and can draw the shape. */
  foxui_stenciltest_in,
  /** We are out of the stencil and can avoid to draw the shape. */
  foxui_stenciltest_out
};

/**
 * Test if the current rectangle has part in the stencil.
 * Return foxui_stenciltest_in if this rectangle has to be displayed.
 * foxui_stenciltest_out if it's totally out.
 */
int foxui_stencil_test(foxui_renderdata *render, const foxui_rectangle *rec);

/* --- Tools box --- */

/**
 * Fox UI rectangle constructor.
 */
foxui_rectangle foxui_sub_construct_rectangle(float x, float y, float w,
                                              float h);

int foxui_write_text(foxui_renderdata *render, const foxui_rectangle text_pos,
                     const char *text, const float font_size);

int foxui_print_image(foxui_renderdata *render, const int image_id,
                      foxui_image_mode mode);

/** Return the size the image will occupy on the screen if it's not stretched or
 * shrunk. */
foxui_vec2 foxui_image_size(foxui_renderdata *render, const int image_id);

/** Get the ratio of the image in the current layer */
foxui_vec2 foxui_image_ratio(foxui_renderdata *render, const int image_id,
                             foxui_image_mode mode);

/* --- Debug --- */

/** Return a string version of the error */
const char *foxui_strerror(const int err);

/** Compute the log from the compilation of th GL program.
 * String is allocated and must be free'd.
 */
char *foxui_gl_program_status(foxui_renderdata *render);

/* --- External functions --- */

/* TODO: Allow to override these functions */

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
/**
 * Allocate a pointer to `data` and read an entire file in it.
 */
int foxui_read_file(const char *file_name, unsigned char **data);

/**
 * Free the pointer filled by `foxui_readfile()`
 */
void foxui_release_read(unsigned **data);

/* --- Internal functions --- */

/**
 * A temp test method
 */
int foxui_draw_rectangle(foxui_renderdata *render, const foxui_rectangle *pos,
                         const foxui_rectangle *uv);

/* --- Debug function --- */

/* TODO: Allow to disable those functions */

void foxui_printf_rectangle(const char *msg, const foxui_rectangle *rec);

/* **********************************************************
--------------------
FOXUI IMPLEMENTATION
--------------------
********************************************************** */
#ifdef SOLEIL__FOXUI_IMPLEMENTATION

#ifndef FOXUI_PRINTERR
#define FOXUI_PRINTERR(...)
#endif

/* TODO: Undef things */

#define FOXUI_ARRAYSIZE(arr) (sizeof(arr) / sizeof(arr[0]))

#ifndef SOLEIL__FOXUI_OPENGL_VERSION
#define SOLEIL__FOXUI_OPENGL_VERSION "#version 330 core\n"
#endif /* SOLEIL__FOXUI_OPENGL_VERSION */
/* TODO: Over-writable by the user */
static const GLchar *vertex_shader_source = {
    SOLEIL__FOXUI_OPENGL_VERSION
    "layout (location = 0) in vec3 vert;				\n"
    "layout (location = 1) in vec2 uv;					\n"
    "uniform mat4 Projection;						\n"
    "									\n"
    "out vec2 fragment_uv;						\n"
    "out vec2 fragment_position;                                        \n"
    "									\n"
    "vec2 rotate(vec2 v, float a) {\n"
    "	float s = sin(a);\n"
    "	float c = cos(a);\n"
    "	mat2 m = mat2(c, -s, s, c);\n"
    "	return m * v;\n"
    "}\n"
    "void main()							\n"
    "{									\n"
    "    gl_Position = Projection * vec4(vert.x, vert.y, vert.z, "
    "1.0);	\n"
    "    //gl_Position = Projection * vec4(vec2(100.5) + rotate(vert.xy, "
    "vert.z), 0.0, 1.0);	\n"
    "    //gl_Position = Projection * vec4(rotate(vert.xy, vert.z), 0.0, "
    "1.0);	\n"
    "    fragment_uv = uv;						\n"
    "    fragment_position = vert.xy;					"
    "	\n"
    "}									\n"
    ""};

/* TODO: Over-writable by the user */
static const GLchar *fragment_shader_source = {
    SOLEIL__FOXUI_OPENGL_VERSION
    "out vec4 fragment_color;                                               \n"
    "in vec2 fragment_position;                                               "
    "\n"
    "in vec2 fragment_uv;			                            \n"
    "						                            \n"
    "uniform sampler2D tex;			                            \n"
    "uniform int sdf;				                            \n"
    "uniform int sdfeasing;				                       "
    "uniform vec4 sdf_values;			                            \n"
    "uniform vec3 sdf_colour;			                            \n"
    "uniform vec3 stencil_circle;		                            \n"
    "						                            \n"
    "float elastic_ease_out(float p) {					    \n"
    "  return sin(-13 * (3.14/2) * (p + 1)) * pow(2, -10 * p) + 1;	    \n"
    "}									    \n"
    "float ElasticEaseIn(float p) {\n"
    "return sin(13 * (3.14/2) * p) * pow(2, 10 * (p - 1));\n"
    "}\n"
    "						                            \n"
    "float circular_ease_in(float p) { return 1 - sqrt(1 - (p * p)); }      \n"
    "						                            \n"
    "float QuinticEaseOut(float p) {\n"
    "  float f = (p - 1);\n"
    "  return f * f * f * f * f + 1;\n"
    "}\n"
    "\n"
    "float BounceEaseOut(float p) {\n"
    "  if (p < 4 / 11.0) {\n"
    "    return (121 * p * p) / 16.0;\n"
    "  } else if (p < 8 / 11.0) {\n"
    "    return (363 / 40.0 * p * p) - (99 / 10.0 * p) + 17 / 5.0;\n"
    "  } else if (p < 9 / 10.0) {\n"
    "    return (4356 / 361.0 * p * p) - (35442 / 1805.0 * p) + 16061 / "
    "1805.0;\n"
    "  } else {\n"
    "    return (54 / 5.0 * p * p) - (513 / 25.0 * p) + 268 / 25.0;\n"
    "  }\n"
    "}\n"
    "float BounceEaseIn(float p) { return 1 - BounceEaseOut(1 - p); }\n"
    "\n"
    "\n"
    "float BounceEaseInOut(float p) {\n"
    "  if (p < 0.5) {\n"
    "    return 0.5 * BounceEaseIn(p * 2);\n"
    "  } else {\n"
    "    return 0.5 * BounceEaseOut(p * 2 - 1) + 0.5;\n"
    "  }\n"
    "}\n"
    "						                            \n"
    "						                            \n"
    "						                            \n"
    "						                            \n"
    "						                            \n"
    "float circle_stencil(vec2 fragPos, vec3 circleDef)			    \n"
    "{									    \n"
    "  vec2 circleCenter = circleDef.xy;				    \n"
    "  float circleRadius = circleDef.z;				    \n"
    "									    \n"
    "  vec2 dist = circleCenter - fragPos;				    \n"
    "  return (dot(dist, dist) < (circleRadius * circleRadius)) ? 1.0 : 0.0;\n"
    "}									    \n"
    "						                            \n"
    "						                            \n"
    "void main()				                            \n"
    "{						                            \n"
    "						                            \n"
    "						                            \n"
    "	float stencil = circle_stencil(fragment_position, stencil_circle);  \n"
    "  	if (sdf == 0) {		      		                            \n"
    "	  fragment_color = texture(tex, fragment_uv);		            \n"
    "	} else {				                            \n"
    "	  float width = sdf_values[0];		                            \n"
    "	  float edge = sdf_values[1];		                            \n"
    "	  float distance = 1.0 - texture(tex, fragment_uv).r;		    \n"
    "	  float v = smoothstep(width, width + edge, distance);		    \n"
    "	  switch (sdfeasing) {		    \n"
    "	    case 0: v = v; break;		    \n"
    "	    case 1: v = ElasticEaseIn(v); break;		    \n"
    "	    case 2: v = elastic_ease_out(v); break;		    \n"
    "	    case 3: v = circular_ease_in(v); break;		    \n"
    "	    case 4: v = QuinticEaseOut(v); break;		    \n"
    "	    case 5: v = BounceEaseOut(v); break;		    \n"
    "	    case 6: v = BounceEaseIn(v); break;		    \n"
    "	    case 7: v = BounceEaseInOut(v); break;		    \n"
    "	  }		    \n"
    "	  float alpha = 1.0 - v;    \n"
    "     fragment_color = vec4(sdf_colour, alpha);               \n"
    "	}					                            \n"
    "	fragment_color.a = fragment_color.a * stencil;                      \n"
    "						                            \n"
    "}                                                                      \n"
    ""};

static void foxui_sub_identity_matrix(float (*mat)[16]) {
  /* Col 1 */
  (*mat)[0] = 1.0f;
  (*mat)[1] = 0.0f;
  (*mat)[2] = 0.0f;
  (*mat)[3] = 0.0f;
  /* Col 2 */
  (*mat)[4] = 0.0f;
  (*mat)[5] = 1.0f;
  (*mat)[6] = 0.0f;
  (*mat)[7] = 0.0f;
  /* Col 3 */
  (*mat)[8] = 0.0f;
  (*mat)[9] = 0.0f;
  (*mat)[10] = 1.0f;
  (*mat)[11] = 0.0f;
  /* Col 4 */
  (*mat)[12] = 0.0f;
  (*mat)[13] = 0.0f;
  (*mat)[14] = 0.0f;
  (*mat)[15] = 1.0f;
}

static void foxui_sub_projection_matrix(float (*mat)[16], int left, int right,
                                        int top, int bottom, int near,
                                        int far) {
  /* Col 1 */
  (*mat)[0] = 2.0f / ((float)(right - left));
  (*mat)[1] = 0.0f;
  (*mat)[2] = 0.0f;
  (*mat)[3] = 0.0f;
  /* Col 2 */
  (*mat)[4] = 0.0f;
  (*mat)[5] = 2.0f / ((float)(top - bottom));
  (*mat)[6] = 0.0f;
  (*mat)[7] = 0.0f;
  /* Col 3 */
  (*mat)[8] = 0.0f;
  (*mat)[9] = 0.0f;
  (*mat)[10] = -2.0f / ((float)(far - near));
  (*mat)[11] = 0.0f;
  /* Col 4 */
  (*mat)[12] = -(((float)(right + left)) / ((float)(right - left)));
  (*mat)[13] = -(((float)(top + bottom)) / ((float)(top - bottom)));
  (*mat)[14] = -(((float)(far + near)) / ((float)(far - near)));
  (*mat)[15] = 1.0f;
}

foxui_rectangle foxui_rectangle_mult(const foxui_rectangle r, const float v) {
  foxui_rectangle rec;

  rec.x = r.x * v;
  rec.y = r.y * v;
  rec.width = r.width * v;
  rec.height = r.height * v;
  rec.rot = 0;
  return rec;
}

foxui_rectangle foxui_rectangle_comp(const foxui_rectangle left,
                                     const foxui_rectangle right) {
  foxui_rectangle ret;
  const float s = foxui_sin(left.rot);
  const float c = foxui_cos(left.rot);
  float scaled_x;
  float scaled_y;

  /* First scale right translation by left scale */
  // scaled_x = right.x * left.width;
  // scaled_y = right.y * left.height;
  scaled_x = right.x;
  scaled_y = right.y;
  /* Then apply the rotation on this scaled vector, and add the
   * left-translation */
  ret.x = left.x + (scaled_x * c - scaled_y * s);
  ret.y = left.y + (scaled_x * s + scaled_y * c);
  /* Compose rotation */
  ret.rot = left.rot + right.rot;
  /* Compose scales? */
  /*ret.width = left.width*/
  ret.width = right.width;
  ret.height = right.height;
  return ret;
}

foxui_rectangle foxui_rectangle_inv(const foxui_rectangle l) {
  foxui_rectangle ret;

  ret.x = -l.x;
  ret.y = -l.y;
  ret.rot = -l.rot;
  /* Skipping scale */
  ret.width = l.width;
  ret.height = l.height;
  return ret;
}

foxui_vec2 foxui_sub_vec2_set(float x, float y) {
  foxui_vec2 vec2;

  vec2.x = x;
  vec2.y = y;
  return vec2;
}

/**
 * Convert a rectangle with position [0, 1] to values with viewport range
 * [viewport_width, viewport_height].
 * Additionally add `pos` offset and size transformations.
 * `pos` can be null if not required.
 */
static foxui_rectangle foxui_sub_convert_rectange(foxui_renderdata *render,
                                                  const foxui_rectangle *rec,
                                                  const foxui_rectangle *pos) {
  foxui_rectangle conv_pos;
  foxui_rectangle off;

  if (pos) {
    off = *pos;
  } else {
    off = foxui_sub_construct_rectangle(0, 0, 1, 1);
  }
  conv_pos.x = (off.x + off.width * rec->x) * render->param->viewport_width;
  conv_pos.y = (off.y + off.height * rec->y) * render->param->viewport_height;
  conv_pos.width = off.width * rec->width * render->param->viewport_width;
  conv_pos.height = off.height * rec->height * render->param->viewport_height;

  return conv_pos;
}

foxui_rectangle foxui_sub_construct_rectangle(float x, float y, float w,
                                              float h) {
  foxui_rectangle pos;
  pos.x = x;
  pos.y = y;
  pos.width = w;
  pos.height = h;

  pos.rot = 0;
  return pos;
}

/** Return the next draw_command and advance the pointer.
 * Do *not* check for out-of-bound. */
static foxui_draw_command *foxui_pop_daw_command(foxui_renderdata *render) {
  // TODO: Custom assert
  assert((render->commands_count + 1) < render->commands_size);
  foxui_draw_command *cmd = &(render->commands_array[render->commands_count]);
  cmd->sdf = 0;
  cmd->stencil = render->current_stencil;
  render->commands_count++;
  return cmd;
}

static void foxui_sub_configure_buffer(foxui_renderdata *render) {
  glGenVertexArrays(1, &(render->vao));
  glGenBuffers(1, &(render->vbo));
  glBindVertexArray(render->vao);
  glBindBuffer(GL_ARRAY_BUFFER, render->vbo);
  glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
  const size_t size = 5 * sizeof(float);
  /* Vertices (3 float) */
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, size, (void *)0);
  glEnableVertexAttribArray(0);
  /* UV (2 floats) */
  /* stride is 3 float x size(float) */
  {
    const size_t stride = 3 * sizeof(float);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, size, (void *)stride);
    glEnableVertexAttribArray(1);
  }
}

static int foxui_configure_unforms(foxui_renderdata *render) {
  const GLuint prog = render->glprogram;
  render->uniform_projection_mat = glGetUniformLocation(prog, "Projection");
  render->uniform_atlas_texture = glGetUniformLocation(prog, "tex");
  render->uniform_sdf = glGetUniformLocation(prog, "sdf");
  render->uniform_sdfeasing = glGetUniformLocation(prog, "sdfeasing");
  render->uniform_sdf_values = glGetUniformLocation(prog, "sdf_values");
  render->uniform_sdf_colour = glGetUniformLocation(prog, "sdf_colour");
  render->uniform_stencil_circle = glGetUniformLocation(prog, "stencil_circle");

  if (render->uniform_projection_mat < 0 || render->uniform_atlas_texture < 0) {
    /* So far, foxui own the shader, so it's considered as an error to not
     * found an uniform */
    return foxui_ret_opengl_error;
  }
  return foxui_ret_success;
}

/* TODO: Clean vertex/fragment resources in case of error */
static int foxui_compile_program(foxui_renderdata *render) {
  GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  if (vertex_shader == 0) {
    return foxui_ret_opengl_error;
  }

  GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
  if (fragment_shader == 0) {
    return foxui_ret_opengl_error;
  }

  render->glprogram = glCreateProgram();
  if (render->glprogram == 0) {
    return foxui_ret_opengl_error;
  }

  glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
  glCompileShader(vertex_shader);

  glShaderSource(fragment_shader, 1, &fragment_shader_source, NULL);
  glCompileShader(fragment_shader);

  glAttachShader(render->glprogram, vertex_shader);
  glAttachShader(render->glprogram, fragment_shader);
  glLinkProgram(render->glprogram);

  GLint success;
  glGetProgramiv(render->glprogram, GL_LINK_STATUS, &success);

  if (!success) {
    return foxui_ret_opengl_error;
  }

  foxui_sub_configure_buffer(render);

  glDeleteShader(vertex_shader);
  glDeleteShader(fragment_shader);

  /* Finally get the uniforms */
  return foxui_configure_unforms(render);
}

/* TODO: Allow to set the corner (left-top) */
int foxui_create(foxui_param *param, foxui_renderdata *render) {
  render->param = param;
  // param->linegap = 1.0f;
  foxui_sub_projection_matrix(&(render->projection_mat), /* Output */
                              0, param->viewport_width,  /* left-right */
                              0, param->viewport_height, /* top-bottom */
                              -1, 1);                    /* near-far */

  const int ret = foxui_compile_program(render);
  if (ret != foxui_ret_success) {
    return ret;
  }

  /* Initialize with some values */
  render->commands_count = 0;
  render->commands_size = 2048;
  render->commands_array = (foxui_draw_command *)foxui_malloc(
      render->commands_size * sizeof(*render->commands_array));
  /* Around one box per command */
  render->vertices_count = 0;
  render->vertices_size = render->commands_size * 4;
  render->vertices_buffer = (float *)foxui_malloc(
      render->vertices_size * sizeof(*render->vertices_buffer));
  /* Around one layer per widget */
  render->widget_count = 0;
  render->widget_size = render->commands_size;
  render->widget_stack = (foxui_rectangle *)foxui_malloc(
      render->widget_size * sizeof(*render->widget_stack));
  render->widget_stack[0].x = 0;
  render->widget_stack[0].y = 0;
  render->widget_stack[0].width = param->viewport_width;
  render->widget_stack[0].height = param->viewport_height;
  render->widget_stack[0].rot = 0;
  foxui_stencil_reset(render);
  render->mouse_pos.x = 0.0f;
  render->mouse_pos.y = 0.0f;
  render->primary_button = foxui_button_released;
  return foxui_ret_success;
}

/* TODO: Allow users to override this method */
/* TODO: Return str from errno if this function fails. */
int foxui_read_file(const char *file_name, unsigned char **data) {
  FILE *file;
  struct stat statb;
  int ret;

  ret = stat(file_name, &statb);
  if (ret != 0) {
    return foxui_ret_io_error;
  }
  file = fopen(file_name, "rb");
  if (file == NULL) {
    return foxui_ret_io_error;
  }
  *data = (unsigned char *)malloc(sizeof(unsigned char *) * statb.st_size);
  if (*data == NULL) {
    return foxui_ret_nomem;
  }
  ret = fread(*data, 1, statb.st_size, file);
  if (ret != statb.st_size) {
    free(*data);
    ret = foxui_ret_io_error;
  } else {
    ret = foxui_ret_success;
  }
  fclose(file);
  return ret;
}

/* TODO: Clean method! */
/* TODO: Find a way to retrieve a glyph and avoid the -32 (e.g. text[i] - 32)
 */
int foxui_font_from_ttf_file(foxui_param *param, foxui_renderdata *render,
                             const char *ttf_file) {
  const int padding = 5;
  const unsigned char one_edge_value = 180;
  const float pixel_dist_scale = 180 / 5.0; /* = 36.0; */
  const wchar_t glyph_range_start = 0x0020;
  const wchar_t glyph_range_end = 0x00FF;
  const int glyphs_count = glyph_range_end - glyph_range_start;
  unsigned char *texture;
  int ret;

  ret = foxui_read_file(ttf_file, &(render->ttf_buffer));
  if (ret != foxui_ret_success) {
    // TODO: True errors
    printf("ReadFile error\n");
    return foxui_ret_configuration_error;
  }
  ret = stbtt_InitFont(&(render->info), render->ttf_buffer, 0);
  if (ret == 0) {
    // TODO: True errors
    printf("InitFont error\n");
    return foxui_ret_configuration_error;
  }

  const float scale = stbtt_ScaleForPixelHeight(&(render->info), 33);

  stbrp_context context;
  int tex_width = 512;
  int tex_height = 512;
  stbrp_node nodes[512];
  stbrp_init_target(&context, tex_width, tex_height, nodes, 512);

  unsigned char **texture_array =
      (unsigned char **)malloc(sizeof(*texture_array) * glyphs_count);
  assert(texture_array);
  stbrp_rect *rects = (stbrp_rect *)malloc(sizeof(*rects) * glyphs_count);
  assert(rects);

  // setlocale(LC_ALL, "");
  for (int i = 0; i < glyphs_count; ++i) {
    int width = 0;
    int height = 0;
    int xoff = 0;
    int yoff = 0;

    texture = stbtt_GetCodepointSDF(
        &(render->info), scale, glyph_range_start + i, padding, one_edge_value,
        pixel_dist_scale, &width, &height, &xoff, &yoff);
    // assert(texture != NULL);
    if (texture == NULL) {
      int c = glyph_range_start + i;
      // printf("Cannot handle char: %C (%i)\n", c, c);
      printf("Cannot handle char: %c (%i)\n", c, c);

      texture_array[i] = NULL;
      rects[i].id = i;
      rects[i].w = 0;
      rects[i].h = 0;
      rects[i].x = 0;
      rects[i].y = 0;
      continue;
    } else {
      int c = glyph_range_start + i;
      // printf("OK for char: %C (%i)\n", c, c);
      printf("OK for char: %c (i=%i)\n", c, i);
    }
    texture_array[i] = texture;
    rects[i].id = i;
    rects[i].w = width;
    rects[i].h = height;
    rects[i].x = 0;
    rects[i].y = 0;
  }

  int ret_pack = stbrp_pack_rects(&context, rects, glyphs_count);
  assert(ret_pack == 1);

  render->glyphs =
      (foxui_glyph *)malloc(sizeof(*render->glyphs) * glyphs_count);
  assert(render->glyphs);

  texture = (unsigned char *)malloc(sizeof(*texture) * tex_width * tex_height);
  for (int i = 0; i < glyphs_count; ++i) {
    stbrp_rect *rect = &(rects[i]);
    const unsigned char *src = texture_array[rect->id]; // rect->id
    assert(rect->was_packed != 0);

    const int c = glyph_range_start + rect->id;
    foxui_glyph *glyph = &(render->glyphs[rect->id]);

    int gx0, gy0, gx1, gy1;
    stbtt_GetCodepointBox(&(render->info), c, &gx0, &gy0, &gx1, &gy1);
    glyph->rect.x = gx0;
    glyph->rect.y = gy1;
    glyph->rect.width = gx1;
    glyph->rect.height = (gy1 - gy0);
    glyph->rect.rot = 0.0f;
    int left_bearing;
    int advance;
    stbtt_GetCodepointHMetrics(&(render->info), c, &(advance), &(left_bearing));
    glyph->advance = advance;
    glyph->left_bearing = left_bearing;

    if (rect->w == 0 || rect->h == 0) {
      continue;
    }

#if 0
      printf("INSERT GLYPH --->%c   | ID(%i) == 'A'-32(%i): %i --> i=%i \n",
             rect->id + 32, rect->id, 'A' - 32, rect->id == 'A' - 32, i);
#endif
    glyph->uvs.x = ((float)rect->x) / ((float)tex_width);
    glyph->uvs.y = ((float)rect->y) / ((float)tex_height);
    glyph->uvs.width = ((float)rect->w) / ((float)tex_width);
    glyph->uvs.height = ((float)rect->h) / ((float)tex_height);

    for (int h = 0; h < rect->h; ++h) {
      const int tex_yoffset = ((rect->y + h) * tex_width);
      const int src_yoffset = (h * rect->w);

      memcpy(texture + tex_yoffset + rect->x, src + src_yoffset,
             sizeof(*texture) * rect->w);
      // for (int w = 0; w < rect->w; ++w)
      //   {
      // 	texture[tex_yoffset + rect->x + w] = src[src_yoffset + w];
      //   }
    }
  }

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glGenTextures(1, &param->atlas_font);
  glBindTexture(GL_TEXTURE_2D, param->atlas_font);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, tex_width, tex_height, 0, GL_RED,
               GL_UNSIGNED_BYTE, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  return foxui_ret_success;
}

static void foxui_sub_resetcounts(foxui_renderdata *render) {
  render->commands_count = 0;
  render->vertices_count = 0;
  render->widget_count = 0;
}

int foxui_render(foxui_renderdata *render) {
  /* For the moment, write on the plain screen */
  /* Disable the depth to be sure to print on it */

  if (render->commands_count == 0) {
    return foxui_ret_success;
  }

  glDisable(GL_DEPTH_TEST);
  {
    glUseProgram(render->glprogram);
    glBindVertexArray(render->vao);
    glBindBuffer(GL_ARRAY_BUFFER, render->vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(*render->vertices_buffer) * render->vertices_count,
                 render->vertices_buffer, GL_DYNAMIC_DRAW);

    glUniformMatrix4fv(render->uniform_projection_mat, 1, GL_FALSE,
                       render->projection_mat);
    glUniform1i(render->uniform_atlas_texture, 0);
    glActiveTexture(GL_TEXTURE0);

    /*printf("There is %zu commands to print\n", render->commands_count);*/
    for (size_t i = 0; i < render->commands_count; ++i) {
      foxui_draw_command *cmd = &(render->commands_array[i]);

      /* TODO: Try to not change the uniforms too much. Include them in the
       * buffer or sort by sdf/non-sdf
       */
      if (cmd->sdf) {
        glBindTexture(GL_TEXTURE_2D, render->param->atlas_font);
        glUniform1i(render->uniform_sdf, 1);
        glUniform1i(render->uniform_sdfeasing, render->param->sdfeasing);
        /*glUniform4f(render->uniform_sdf_values, 0.45f, 0.01f, 1, 1);*/
        glUniform4f(render->uniform_sdf_values, render->param->sdf_values[0],
                    render->param->sdf_values[1], 1, 1);
        glUniform3f(render->uniform_sdf_colour, 0, 0, 0);
      } else {
        glBindTexture(GL_TEXTURE_2D, render->param->atlas_texture);
        glUniform1i(render->uniform_sdf, 0);
      }

      const foxui_stencil *stencil = &(cmd->stencil);
      if (stencil->type != 0) {
        printf("Setting stencil with radius: %f (pos= %f, %f)\n",
               stencil->extent.radius, stencil->position.x,
               stencil->position.y);
        glUniform3f(render->uniform_stencil_circle, stencil->position.x,
                    stencil->position.y, stencil->extent.radius);
      } else {
        /* Stencil disabled. For the moment we bind a BIG circle stencil so we
         * a are sure we are in. */
        const float numberBigEnough =
            render->param->viewport_width * render->param->viewport_width +
            render->param->viewport_height * render->param->viewport_height;
        glUniform3f(render->uniform_stencil_circle, 0, 0, numberBigEnough);
      }

      glDrawArrays(GL_TRIANGLE_STRIP, cmd->vertices_start, cmd->count);
    }
  }
  glEnable(GL_DEPTH_TEST);

  foxui_sub_resetcounts(render);

  return foxui_ret_success;
}

int foxui_widget_textbox(foxui_renderdata *render, const foxui_rectangle *pos,
                         const char *text) {
  // const foxui_rectangle conv_pos = foxui_sub_convert_rectange(render, pos);
  // const foxui_rectangle conv_pos = *pos;
  // const float                 posx     = conv_pos.x;
  // const float                 posy     = conv_pos.y;

  if (render->param->lnf_list_count <= foxui_widgetlist_textbox) {
    return foxui_ret_configuration_error;
  }
  const foxui_widget_lnf *textbox_lnf =
      &(render->param->lnf_list[foxui_widgetlist_textbox]);
  for (size_t i = 0; i < textbox_lnf->nb_parts; i++) {
    const foxui_rectangle part = textbox_lnf->parts_size[i];
    const foxui_rectangle uv = textbox_lnf->parts_uv[i];
    const foxui_rectangle p2 = foxui_sub_convert_rectange(render, &part, pos);
    const int ret = foxui_draw_rectangle(render, &p2, &uv);

    if (ret != foxui_ret_success) {
      return ret;
    }

    if (i == foxui_widgetpart_content) {
      const foxui_rectangle text_pos = p2;
      foxui_write_text(render, text_pos, text, 64);
      render->widget_count++;
      if (render->widget_count >= render->widget_size) {
        return foxui_ret_nomem;
      }
      render->widget_stack[render->widget_count] = text_pos;
    }
  }
  return foxui_ret_success;
}

foxui_rectangle foxui_last_layer(foxui_renderdata *render) {
  /* widget_count is the number of added layer after the initial one. So this
   * is indeed `widget_count` and not `widget_count - 1`. A value of 0 means
   * it will returns the first layer. */
  return render->widget_stack[render->widget_count];
}

int foxui_set_cell(foxui_renderdata *render, int col, int row, int width,
                   int height) {
  const foxui_rectangle prev = foxui_last_layer(render);
  const float col_size = (prev.width / 12.0f);
  const float row_size = (prev.height / 6.0f);

  render->widget_count++;
  if (render->widget_count >= render->widget_size) {
    return foxui_ret_nomem;
  }
  assert(render->widget_count > 0);
  render->widget_stack[render->widget_count].x = prev.x + col * col_size;
  render->widget_stack[render->widget_count].y = prev.y + row * row_size;
  render->widget_stack[render->widget_count].width = width * col_size;
  render->widget_stack[render->widget_count].height = height * row_size;
  render->widget_stack[render->widget_count].rot = 0;
  return foxui_ret_success;
}

// static foxui_rectangle foxui_sub_rotate(const foxui_rectangle* rec, float
// a)
// {
//   float           s   = sin(a);
//   float           c   = cos(a);
//   foxui_rectangle res = *rec;

//   res.x = rec->x * c - rec->y * s;
//   res.y = rec->x * s + rec->y * c;
//   return res;
// }

int foxui_set_cell_r(foxui_renderdata *render, int col, int row, int width,
                     int height, float r, foxui_vec2 *pivot) {
  const foxui_rectangle prev = foxui_last_layer(render);
  const float col_size = (prev.width / 12.0f);
  const float row_size = (prev.height / 6.0f);
  foxui_rectangle *widget;
  foxui_rectangle pivotr;

  render->widget_count++;
  if (render->widget_count >= render->widget_size) {
    return foxui_ret_nomem;
  }
  assert(render->widget_count > 0);

  widget = &(render->widget_stack[render->widget_count]);
  widget->x = col * col_size;
  widget->y = row * row_size;
  widget->rot = r;
  widget->width = width * col_size;
  widget->height = height * row_size;
  if (pivot) {
    pivotr.x = pivot->x;
    pivotr.y = pivot->y;
    pivotr.width = widget->width;
    pivotr.height = widget->height;

    *widget = foxui_rectangle_comp(pivotr, *widget);
  }

  *widget = foxui_rectangle_comp(prev, *widget);

  if (pivot) {
    *widget = foxui_rectangle_comp(*widget, foxui_rectangle_inv(pivotr));
  }

  printf("%p) Parent rot=%f, current=%f\n", (void *)widget, prev.rot, r);
  printf("%p) Rectangle(tr=[%f, %f], wh=[%f, %f]\n", (void *)widget, widget->x,
         widget->y, widget->width, widget->height);
  return foxui_ret_success;
}

static void foxui_sub_apply_transform(foxui_renderdata *render,
                                      const foxui_rectangle *previous,
                                      foxui_rectangle *widget,
                                      foxui_vec2 *pivot) {
  foxui_rectangle pivotr;

  if (pivot) {
    pivotr.x = pivot->x;
    pivotr.y = pivot->y;
    pivotr.width = widget->width;
    pivotr.height = widget->height;
    pivotr.rot = 0.0f;

    *widget = foxui_rectangle_comp(pivotr, *widget);
  }

  *widget = foxui_rectangle_comp(*previous, *widget);

  if (pivot) {
    *widget = foxui_rectangle_comp(*widget, foxui_rectangle_inv(pivotr));
  }
  return;
}

int foxui_layer_push(foxui_renderdata *render, foxui_rectangle rec,
                     foxui_vec2 *pivot) {
  const foxui_rectangle prev = foxui_last_layer(render);
  foxui_rectangle *widget;
  foxui_rectangle pivotr;

  render->widget_count++;
  if (render->widget_count >= render->widget_size) {
    return foxui_ret_nomem;
  }
  assert(render->widget_count > 0);

  widget = &(render->widget_stack[render->widget_count]);
  *widget = rec;
  foxui_sub_apply_transform(render, &prev, widget, pivot);
  return foxui_ret_success;
}

/* TODO: FIXME: Do it reaaly relative if needed. */
/* TODO: FIXME: rename as foxui_layer_push_relative. */
int foxui_push_relative_layer(foxui_renderdata *render, foxui_rectangle rec,
                              foxui_vec2 *pivot) {
  const foxui_rectangle prev = foxui_last_layer(render);
  foxui_rectangle *widget;
  foxui_rectangle pivotr;

  render->widget_count++;
  if (render->widget_count >= render->widget_size) {
    return foxui_ret_nomem;
  }
  assert(render->widget_count > 0);

  widget = &(render->widget_stack[render->widget_count]);
  *widget = rec; // TODO: FIXME: Seems not to be relative.
  foxui_sub_apply_transform(render, &prev, widget, pivot);
  return foxui_ret_success;
}

int foxui_layer_pop(foxui_renderdata *render) {
  if (render->widget_count == 0) {
    return foxui_ret_configuration_error;
  }
  render->widget_count--;
  return foxui_ret_success;
}

int foxui_layer_hovered(foxui_renderdata* render)
{
  foxui_rectangle rec = foxui_last_layer(render);

  if ((render->mouse_pos.x < rec.x) ||
      (render->mouse_pos.x > (rec.x + rec.width)) ||
      (render->mouse_pos.y < rec.y) ||
          (render->mouse_pos.y > (rec.y + rec.height))) {
    return 0;
  }
  return 1;
}

int foxui_layer_clicked(foxui_renderdata* render)
{
  if (render->primary_button != foxui_button_just_released) {
    return 0;
  }
  return foxui_layer_hovered(render);
}

/* TODO: Allow a widget to be dragged even if the user quit the layer zone while
 * button pressed. */
int foxui_layer_dragged(foxui_renderdata* render, float* factor)
{
  const foxui_rectangle rec = foxui_last_layer(render);
  
  if (render->primary_button != foxui_button_just_pressed
      && render->primary_button != foxui_button_pressed) {
    return 0;
  }
  if (foxui_layer_hovered(render)) {
    if (factor) {
      *factor = (render->mouse_pos.x - rec.x) / rec.width;
    }
    return 1;
  }
  return 0;
}

  
int foxui_stencil_circle(foxui_renderdata *render, foxui_vec2 position,
                         float radius) {
  foxui_rectangle *layout;

  layout = &(render->widget_stack[render->widget_count]);

  /* TODO: FIXME Position is not correct. */
  render->current_stencil.type = 1;
  render->current_stencil.position.x = layout->x + position.x;
  render->current_stencil.position.y = layout->y + position.y;
  render->current_stencil.extent.radius = radius;
  return foxui_ret_success;
}

int foxui_stencil_layer(foxui_renderdata *render) {
  foxui_rectangle rec = foxui_last_layer(render);

  return foxui_stencil_square(render, rec);
}

int foxui_stencil_square(foxui_renderdata *render, const foxui_rectangle rec) {
  /* TODO: Check if Position is correct (not tested yet). */

  render->current_stencil.type = 2; /* TODO: Use enum */
  render->current_stencil.position.x = rec.x;
  render->current_stencil.position.y = rec.y;
  render->current_stencil.extent.size.x = rec.width;
  render->current_stencil.extent.size.y = rec.height;
  return foxui_ret_success;
}

typedef int (*foxui_stencil_test_m)(foxui_renderdata *render,
                                    const foxui_rectangle *rec);

static int foxui_stencil_test_pass(foxui_renderdata *render,
                                   const foxui_rectangle *rec) {
  return foxui_stenciltest_in;
}

int foxui_stencil_test_circle(foxui_renderdata *render,
                              const foxui_rectangle *rec) {
  const foxui_vec2 center = render->current_stencil.position;
  const float radius = render->current_stencil.extent.radius;
  int out;

  out = (rec->x > center.x + radius) ||
        ((rec->x + rec->width) < center.x - radius) ||
        (rec->y > center.x + radius) ||
        ((rec->y + rec->height) < center.y - radius);
  return (out) ? foxui_stenciltest_in : foxui_stenciltest_out;
}

int foxui_stencil_test_rec(foxui_renderdata *render,
                           const foxui_rectangle *rec) {
  const foxui_vec2 corner = render->current_stencil.position;
  const foxui_vec2 extent = render->current_stencil.extent.size;
  int out;

  out = (rec->x > corner.x + extent.x) || ((rec->x + rec->width) < corner.x) ||
        (rec->y > corner.y + extent.y) || ((rec->y + rec->height) < corner.y);
  return (out) ? foxui_stenciltest_in : foxui_stenciltest_out;
}

static foxui_stencil_test_m foxui_stencil_test_array[] = {
    foxui_stencil_test_pass,
    foxui_stencil_test_circle,
    foxui_stencil_test_rec,
};

int foxui_stencil_test(foxui_renderdata *render, const foxui_rectangle *rec) {
  /*if (render->current_stencil == 0) {
    return foxui_stenciltest_in;
    }*/
  assert(render->current_stencil.type >= 0);
  assert(render->current_stencil.type <=
         (int)FOXUI_ARRAYSIZE(foxui_stencil_test_array));
  return foxui_stencil_test_array[render->current_stencil.type](render, rec);
}

int foxui_stencil_reset(foxui_renderdata *render) {
  render->current_stencil.type = 0;
  return foxui_ret_success;
}

/**
 * Returns the index position in the text of the next time it's required to
 * break the line. Either because it's  \n, or because it does not fit
 * anymore. On the second case, the break is operated at the previous space
 *  preferably or on the word.
 */
static size_t foxui_sub_next_breakline(const char *text, const size_t start,
                                       const size_t count, const float pos_x,
                                       const float max_x, float ttf_scale,
                                       const foxui_renderdata *render) {
  size_t i = start;
  size_t previous_space = -1;
  float accum_x = pos_x;

  while (i < count) {
    const int c = text[i] - 32;
    const foxui_glyph *glyph = &(render->glyphs[c]);
    float next_accum_x;

    if (text[i] == '\n') {
      return i;
    } else if (text[i] == ' ') {
      previous_space = i;
    }
    next_accum_x = accum_x + ((glyph->rect.width) * ttf_scale);
    if (next_accum_x >= max_x) {
      return (previous_space != (size_t)-1) ? previous_space : i;
    }
    accum_x = next_accum_x;
    i++;
  }
  return -1;
}

/**
 * Compute the Line height (space between the top of two lines) and
 * prepare the baseline.
 */
static float foxui_sub_compute_line_height(foxui_renderdata *render,
                                           const float ttf_scale,
                                           float *baseline) {
  int ascent;
  int descent;
  int linegap;

  stbtt_GetFontVMetrics(&(render->info), &ascent, &descent, &linegap);
  if (baseline) {
    *baseline = (ascent * ttf_scale);
  }
  return ((ascent - descent + linegap) * render->param->linegap * ttf_scale);
}

static int foxui_sub_draw_glyph(const foxui_glyph *glyph, const float ttf_scale,
                                const float advance_x, const float baseline,
                                foxui_renderdata *render) {
  foxui_rectangle rect;

  rect = foxui_rectangle_mult(glyph->rect, ttf_scale);
  rect.x += advance_x;
  rect.y = baseline - rect.y;
  int ret = foxui_draw_rectangle(render, &rect, &(glyph->uvs));
  if (ret == foxui_ret_success) {
    foxui_draw_command *prev =
        &(render->commands_array[render->commands_count - 1]);
    prev->sdf = 1;
  }
  return ret;
}

/* TODO: Kern advance */
/* TODO: Scissor max height */
int foxui_write_text(foxui_renderdata *render, const foxui_rectangle text_pos_2,
                     const char *text, const float font_size) {
  const foxui_rectangle text_pos = render->widget_stack[render->widget_count];
  const float max_advance = text_pos.x + text_pos.width;
  size_t count = strlen(text);
  float advance_x = text_pos.x;
  float baseline;
  const float ttf_scale = stbtt_ScaleForPixelHeight(&(render->info), font_size);
  float line_height =
      foxui_sub_compute_line_height(render, ttf_scale, &baseline);
  size_t next_breakline = foxui_sub_next_breakline(
      text, 0, count, text_pos.x, max_advance, ttf_scale, render);
  int ret;

  baseline += text_pos.y;
  for (size_t i = 0; i < count; ++i) {
    const foxui_glyph *glyph = &(render->glyphs[text[i] - 32]);

    if (i == next_breakline) {
      advance_x = text_pos.x;
      baseline += line_height;
      next_breakline = foxui_sub_next_breakline(text, i + 1, count, text_pos.x,
                                                max_advance, ttf_scale, render);
      if (isspace(text[i])) {
        continue;
      }
    }
    if (isspace(text[i])) {
      advance_x += (glyph->advance * ttf_scale);
      continue;
    }
    ret = foxui_sub_draw_glyph(glyph, ttf_scale, advance_x, baseline, render);
    if (ret != foxui_ret_success) {
      return ret;
    }
    advance_x += (glyph->advance * ttf_scale);
  }
  return foxui_ret_success;
}

static int foxui_print_image_fit_width(foxui_renderdata *render,
                                       foxui_rectangle *layout,
                                       foxui_rectangle *img) {
  const float image_width = img->width * render->param->atlas_width;
  const float ratio = layout->width / image_width;
  foxui_rectangle rec = *layout;
  rec.height = img->height * render->param->atlas_height * ratio;
  return foxui_draw_rectangle(render, &rec, img);
}

static int foxui_print_image_fit_height(foxui_renderdata *render,
                                        foxui_rectangle *layout,
                                        foxui_rectangle *img) {
  const float image_height = img->height * render->param->atlas_height;
  const float ratio = layout->height / image_height;
  foxui_rectangle rec = *layout;
  rec.width = img->width * render->param->atlas_width * ratio;
  return foxui_draw_rectangle(render, &rec, img);
}

static int foxui_print_image_keep_ratio(foxui_renderdata *render,
                                        foxui_rectangle *layout,
                                        foxui_rectangle *img) {
  foxui_rectangle *prev;
  float ratio_width;
  float ratio_height;
  foxui_rectangle rec = *layout;

  /* FIXME: we may push an image on the main layer! */
  // assert(render->widget_count > 0);
  // prev         = &(render->widget_stack[render->widget_count - 1]);
  prev = &(render->widget_stack[render->widget_count]);
  ratio_width = prev->width / render->param->viewport_width;
  ratio_height = prev->height / render->param->viewport_height;
  // rec.width = img->width * render->param->viewport_width * ratio_width;
  // rec.height = img->height * render->param->viewport_height * ratio_height;
  rec.width = img->width * render->param->atlas_width * ratio_width;
  rec.height = img->height * render->param->atlas_height * ratio_height;
  return foxui_draw_rectangle(render, &rec, img);
}

static int foxui_print_image_true_size(foxui_renderdata *render,
                                       foxui_rectangle *layout,
                                       foxui_rectangle *img) {
  foxui_rectangle rec = *layout;

  rec.width = img->width * render->param->atlas_width;
  rec.height = img->height * render->param->atlas_height;
  return foxui_draw_rectangle(render, &rec, img);
}

int foxui_print_image(foxui_renderdata *render, const int image_id,
                      foxui_image_mode mode) {
  foxui_rectangle *layout;
  foxui_rectangle *img;

  /* TODO: Replace switch with a function pointer array */
  if (image_id >= render->param->user_image_count) {
    return foxui_ret_configuration_error;
  }
  layout = &(render->widget_stack[render->widget_count]);
  img = &(render->param->user_images[image_id]);
  switch (mode) {
  case foxui_image_extend:
    return foxui_draw_rectangle(render, layout, img);
  case foxui_image_fit_width:
    return foxui_print_image_fit_width(render, layout, img);
  case foxui_image_fit_height:
    return foxui_print_image_fit_height(render, layout, img);
  case foxui_image_keep_ratio:
    return foxui_print_image_keep_ratio(render, layout, img);
  case foxui_image_true_size:
    return foxui_print_image_true_size(render, layout, img);
  default:
    assert(0 && "Undefined mode.");
    return foxui_draw_rectangle(render, layout, img);
  }
}

/* TODO: If this function makes the job it can replace all the function
 * foxui_print_image_... */
foxui_vec2 foxui_image_ratio(foxui_renderdata *render, const int image_id,
                             foxui_image_mode mode) {
  foxui_vec2 vec;

  switch (mode) {
  case foxui_image_keep_ratio: {
    foxui_rectangle *prev;
    float ratio_width;
    float ratio_height;
    foxui_rectangle rec = foxui_last_layer(render);

    prev = &(render->widget_stack[render->widget_count]);
    ratio_width = prev->width / render->param->viewport_width;
    ratio_height = prev->height / render->param->viewport_height;
#if 0
    vec.x = img->width * render->param->atlas_width * ratio_width;
    vec.y = img->height * render->param->atlas_height * ratio_height;
#else
    vec.x = ratio_width;
    vec.y = ratio_height;
#endif
  } break;

  case foxui_image_fit_height: {
    foxui_rectangle *img = &(render->param->user_images[image_id]);
    const float image_height = img->height * render->param->atlas_height;
    foxui_rectangle rec = foxui_last_layer(render);
    const float ratio = rec.height / image_height;
    // rec.width = img->width * render->param->atlas_width * ratio;
    vec.x = ratio;
    vec.y = 1.0f;
  } break;

  case foxui_image_true_size: {
    vec.x = 1.0f;
    vec.y = 1.0f;
  } break;

  case foxui_image_extend:    /* TODO: Not implemented yet. */
  case foxui_image_fit_width: /* TODO: Not implemented yet. */
  default:
    assert(0 && "Undefined mode.");
    vec.x = 1.0f;
    vec.y = 1.0f;
  }
  return vec;
}

foxui_vec2 foxui_image_size(foxui_renderdata *render, const int image_id) {
  foxui_vec2 ret;
  const foxui_rectangle *img = &(render->param->user_images[image_id]);

  ret.x = img->width * render->param->atlas_width;
  ret.y = img->height * render->param->atlas_height;
  return ret;
}

static void foxui_sub_buffer_insert(float *buffer, size_t *count, const float x,
                                    const float y, const float z, const float u,
                                    const float v) {
  buffer = &(buffer[*count]);
  *buffer++ = x;
  *buffer++ = y;
  *buffer++ = z;
  *buffer++ = u;
  *buffer++ = v;

  *count += 5;
}

int foxui_draw_rectangle(foxui_renderdata *render, const foxui_rectangle *pos,
                         const foxui_rectangle *uv) {
  const size_t tri_count = 4;

  /* 4 points x 3 float (vertices) + 4 points * 2 float (uv) */
  const size_t required_count = tri_count * (3 + 2);
  if (((render->vertices_count + required_count) >= render->vertices_size) ||
      ((render->commands_count + 1) >= render->commands_size)) {
    /* TODO: Or reallocate */
    return foxui_ret_nomem;
  }

  if (foxui_stencil_test(render, pos) == foxui_stenciltest_out) {
    return foxui_ret_success;
  }

  foxui_draw_command *cmd = foxui_pop_daw_command(render);
  if (render->commands_count == 1) {
    cmd->vertices_start = 0;
  } else {
    /* The render->commands_count pointer has advanced and point to the first
     * free command. So -1 is the current command we are using, and -2 is the
     * previous one. */
    foxui_draw_command *prev =
        &(render->commands_array[render->commands_count - 2]);
    cmd->vertices_start = prev->vertices_start + prev->count;
  }
  cmd->count = tri_count;

  float *buffer = render->vertices_buffer;
  size_t *count = &(render->vertices_count);
  const float height = pos->y + pos->height;
  const float width = pos->x + pos->width;
  const float uheight = uv->y + uv->height;
  const float uwidth = uv->x + uv->width;

  const float vertices[][2] = {
      {0, 0}, /* top    left */
      {0, 1}, /* bottom left */
      {1, 0}, /* top    right */
      {1, 1}, /* bottom right */
  };          // TODO: scale
  float s = foxui_sin(pos->rot);
  float c = foxui_cos(pos->rot);

#if 1
  for (int i = 0; i < 4; i++) {
    float x =
        vertices[i][0] * pos->width * c - vertices[i][1] * pos->height * s;
    float y =
        vertices[i][0] * pos->width * s + vertices[i][1] * pos->height * c;
    float uvx = vertices[i][0] * uv->width;
    float uvy = vertices[i][1] * uv->height;

    foxui_sub_buffer_insert(buffer, count, pos->x + x, pos->y + y, 0.0f,
                            uv->x + uvx, uv->y + uvy);
  }
#else
  // TODO: Handmade try

  /* Top left: */
  foxui_sub_buffer_insert(buffer, count, pos->x, pos->y, 0.0f, uv->x, uv->y);
  /* Bottom left: */
  foxui_sub_buffer_insert(buffer, count, pos->x - s, pos->y + c, 0.0f, uv->x,
                          uheight);
  /* Top right: */
  foxui_sub_buffer_insert(buffer, count, pos->x + c, pos->y + s, 0.0f, uwidth,
                          uv->y);
  /* Bottom right: */
  foxui_sub_buffer_insert(buffer, count, pos->x + (c - s), pos->y + (s + c),
                          0.0f, uwidth, uheight);
#endif
  return foxui_ret_success;
}

void foxui_printf_rectangle(const char *msg, const foxui_rectangle *rec) {
  printf("%s Rectangle(tr=[%f, %f], wh=[%f, %f] rot=%f)\n", msg, rec->x, rec->y,
         rec->width, rec->height, rec->rot);
}

const char *foxui_strerror_table[] = {"Success", "OpenGL error",
                                      "Configuration error", "IO error",
                                      "Not enough memory"};

const char *foxui_strerror(const int err) {
  if (err < 0 || err > (int)FOXUI_ARRAYSIZE(foxui_strerror_table)) {
    return "Invalid parameter in foxui_strerror";
  }
  return foxui_strerror_table[err];
}

char *foxui_gl_program_status(foxui_renderdata *render) {
  GLint is_compiled = 0;
  GLint max_length = 0;
  int message_max_length;
  char *msg;
  const char *compiledstr;
  GLuint shader = render->glprogram;

  glGetShaderiv(shader, GL_COMPILE_STATUS, &is_compiled);
  compiledstr = (is_compiled) ? "SUCCESS: " : "FAILED: ";
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &max_length);
  /* TODO: check return */
  message_max_length = strlen("SUCCESS: ") + max_length;
  msg = (char *)foxui_malloc(sizeof(*msg) * (message_max_length));
  bzero(msg, sizeof(*msg) * (message_max_length));
  strcat(msg, compiledstr);
  /* `maxLength` may be equal zero if we have no information
   * The `maxLength` includes the NULL character, so a size of 1 means
   * an empty string.
   */
  /* TODO custom strlen */
  if (max_length > 0) {
    glGetShaderInfoLog(shader, max_length, &max_length,
                       &(msg[strlen(msg) - 2]));
  }
  return msg;
}

#endif /* SOLEIL__FOXUI_IMPLEMENTATION */

#ifdef __cplusplus
}
#endif

#endif /* SOLEIL__FOX_UI_H_ */
